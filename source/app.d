import std.stdio;
import bitarray:BitArray;
void main()
{
    "! void package here".writeln;
    "! check subpackages".writeln;
}

unittest {
    auto bits = new BitArray([1,2,123]);
    bits.writeln;
    bits.writeln;
    bits ~= true;
    bits.writeln;
    bits ~= false;
    bits.writeln;
    bits ~= [true, false, true];
    bits.writeln;
    auto a = bits.toString;
    File save = File("/tmp/bitscontent.dat", "wb");
    bits.write(save);
    save.close;

    File _read = File("/tmp/bitscontent.dat", "rb"); 
    BitArray bbits = new BitArray([]);
    bbits.read(_read);
    auto b = bbits.toString;
    assert (a == b, a ~ " != " ~ b);
}

