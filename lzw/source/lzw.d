
auto initTable(T)(ref T table){
    foreach(i; 0..256){
        static if(is(T==int[string]))
            table[""~cast(char)i] = i;
        else static if(is(T==string[int]))
            table[i] = ""~cast(char)i;

    }
    return table;
}

auto encode(string input) 
in(input.length > 0)
{
    int[string] table;
    table.initTable;
    string p = "";
    string c = "";
    
    p ~= input[0];
    int code = 256;
    int [] output = [];
    output.reserve(input.length);
    foreach(i,_; input){
        if (i+1 != input.length )
        {
            c~= input[i+1];
        }
        if(p~c in table){
            p ~= c;
        }
        else {
            output ~= table[p]; 
            table[p ~ c] = code++;
            p = c;
        }
        c = "";
    }
    output~= table[p];
    return output; 
}

auto decode(int[] input){
    string[int] table;
    table.initTable;
    int old = input[0];
    string s = table[old];
    string output = "";
    string c = "";
    c ~= s[0];
    output ~= s;
    int count = 256;
    foreach(n; input[1..$]){
        if(n !in table){
            s = table[old];
            s ~= c;
        }
        else{
            s = table[n];
        }
        output~=s;
        c = "";
        c ~= s[0];
        table[count] = table[old]~c;
        count++;
        old = n;
    }
    return output;

}
unittest{
    import std;
    auto A = 100.iota
        .map!( _ =>"djfuhruhfiuhfruruhfuhrufhruhfurhfuhrpouhoPUFHAPOUSDHF")
        .join("abc");
    auto b = A.encode;
    auto a = b.decode;
    assert(a==A);
    writeln(a.length,'\n',
            b.length*int.sizeof);
}
