import std;

static import lzw;

auto compres(string input, string output){
    auto data = cast(string)input.read();
    auto f = File(output, "wb");
    f.rawWrite(lzw.encode(data));
}
auto decompres(string input, string output){
    File f = input.File;
    auto data = new int[f.size/4];
    f.rawRead(data);
    f.close;
    f = File(output, "w");
    f.rawWrite(lzw.decode(data));
    f.close;
}

void main(string[] argv)
{
    string fileName  = "";
    string archiveName = "";
    string outputName = "a.lzw";
    auto params = getopt(argv, 
            "c", "File to compres", &fileName, 
            "d", "File to decompres", &archiveName,
            "o", "path to write result", &outputName,
            );
    auto printHelp(){
        defaultGetoptPrinter("Archivetor powered by haffman code",
            params.options);
    }

    if (params.helpWanted){
        printHelp;
        return;
    }
    
    if(fileName!="" && archiveName!=""){
        assert(0 , "Cannot compres and decompres simultaneously");
    }
    if (fileName!=""){
        fileName.compres(outputName);
        return;
    }
    if(archiveName != ""){
        archiveName.decompres(outputName);
        return;
    }

    printHelp;
}
