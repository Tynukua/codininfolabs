import bitarray;
import std.stdio;

auto xor(BitArray dividend, BitArray divider)
    in(dividend.length>= divider.length)
{
    immutable codeLen = dividend.length - divider.length;
    auto i = dividend.firstBit;
    while (i <= codeLen){
        foreach(j; 0..divider.length){
 //           writeln(dividend[i+j], "^",divider[j],"=",dividend[i+j]^divider[j]);
            dividend[i+j] = dividend[i+j]^divider[j];
        }
        i = dividend.firstBit;
        dividend.writeln;
    }
}


auto firstBit(BitArray a) {
    foreach(i; 0..a.length){
        if(a[i]) return i;
    }
    return a.length;
}

