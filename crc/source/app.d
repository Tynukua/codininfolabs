import std.stdio;
import std.conv;
import crc;
import bitarray;

alias chars = char[];
alias bytes = ubyte[];

void main()
{
    auto polynom = new BitArray([0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d]);
    auto rawData = readln.to!chars;
    //auto data = new BitArray(cast(ubyte[]) rawData);
    auto data = new BitArray([0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d]);
    data.writeln;
    polynom.writeln;
    xor(data, polynom);
    data.writeln;
    ulong checksum = (cast(ulong[])data.value[$-8..$])[0];
    writefln!"%x"(checksum);
}
