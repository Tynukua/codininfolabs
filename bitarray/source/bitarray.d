module bitarray;

import std:Alias;
import std.conv:to;
import std.stdio;


/// class for manupulate bits in array
class BitArray{
    /// variable with content
    ubyte[] value;
    /// count of bits
    ulong _length;

    /// States for iterator
    ulong cur = 0;
    
    this(ubyte[] array){
        value = array;
        _length = array.length*8;
    }

    @property
    auto length(){
        return _length;
    }
    @property
    auto length(ulong l){
        if (l.divWithLeft> _length.divWithLeft){
            auto c = l.divWithLeft - _length.divWithLeft;
            foreach(_; 0..c){
                value ~= 0;
            }
        }
        _length = l;
    }

    bool opIndexAssign(bool v, ulong bit) {
        auto mask = 0b1 << (7-bit % 0x8);
        if(v){
            value[bit / 0x8] |= mask;
        }
        else{
            value[bit / 0x8] &= ~mask;
        }
        return v;
    }
    bool opIndex(ulong bit){
        ubyte b = value[bit / 0x8];
        auto mask = 0b1 << (7 - bit % 0x8);
        if (mask & b)
            return true;
        return false;
    }

    bool empty(){
        auto r = cur == length; 
        if (r) cur = 0;
        return r;
    }

    bool front(){
        return this[cur];
    }

    void popFront(){
        if(empty)
            cur = 0;
        else
            cur++;
    }
    
    void opOpAssign(string op, T)(T value){
        static assert (op == "~", "op not suported");
        alias bools = bool[];
        static if(is(T == bools)){
            foreach (v; value){
                addBit(v);
            }
        }
        else static if (is(T == bool)){
            addBit(value);
        }
        else static assert(0, "not supported type " ~ T.stringof );
    }

    private void addBit(bool bit){
        length = length + 1;
        this[length-1] = bit;
    }

    /// represnt bitarray as ninary number e.g. "0b1000001"   
    override string toString() {
        string result = "0b";
        foreach(bit; this) {
            if(bit){
                result ~= "1";
            }
            else {
                result ~= "0";
            }

        }
        return result;
    }

    /// Writes count of bis and bytes
    void write(File output) const{
        ulong[1] _len = [_length];
        output.rawWrite(_len);
        output.rawWrite(value);
    }

    /// reads count of bits and bytes
    void read(File input) {
        ulong[1] _len;
        input.rawRead(_len);
        auto len = _len[0];
        ubyte[] v = new ubyte[len.divWithLeft];
        input.rawRead(v);

        value = v;
        _length = len;
    }
}

auto divWithLeft(ulong t, ulong p = 8){
    return t/p + (t%p?1:0);
}

