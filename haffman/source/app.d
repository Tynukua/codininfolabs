import std;
import std.getopt;

import haffman;

void main(string[] argv)
{
    string fileName  = "";
    string archiveName = "";
    string outputName = "a.haff";
    ulong byteLen = 1;
    auto params = getopt(argv, 
            "c", "File to compres", &fileName, 
            "d", "File to decompres", &archiveName,
            "o", "path to write result", &outputName,
            "l", "length of letter", &byteLen);

    auto printHelp(){
        defaultGetoptPrinter("Archivetor powered by haffman code",
            params.options);
    }

    if (params.helpWanted){
        printHelp;
        return;
    }
    
    if(fileName!="" && archiveName!=""){
        assert(0 , "Cannot compres and decompres simultaneously");
    }
    if (fileName!=""){
        fileName.compres(outputName, byteLen);
        return;
    }
    if(archiveName != ""){
        archiveName.decompres(outputName);
        return;
    }

    printHelp;
}
