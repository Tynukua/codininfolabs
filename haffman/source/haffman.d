module haffman;

import std.conv;
import std.algorithm;
import std.array;
import std: tuple;
import std:writeln;
import core.stdc.string: strncmp;
import bitarray;

alias ubytes = immutable(ubyte)[];
alias Counts = ulong[ubytes];
alias bools = bool[];

class Node {
    bool content = false;
    ubytes value;
    ulong count;
    Node left, right;

    this (Node r, Node l) 
    in(r !is null && l !is null)
    {
        left = l;
        count = r.count + l.count;
        right = r;
    }

    this(ulong count){
        this.count = count;
    }
    
    this(ubytes v, ulong count){
        this(count);
        this.value = v;
        content = true;
    }

    override string toString(){
        if (content){
            return "'"~ value.to!string ~"'";
        }else
        {
            string t = "";
            t~= count.to!string ~ " ";
            if(left !is null){
                t~= left.toString;
            }
            if(right!is null){
                t~= right.toString;
            }
            return t;           

        }
    }
}


auto countBytes(ubytes v, ulong byteLen= 1){
    Counts counts;
    foreach(i; 0..v.length/byteLen) {
        auto c = v[i*byteLen..(i+1)*byteLen];
        counts[c]++;
    }
    return counts;
}

auto buildHaffmanTree(T)(T v){
    Counts counts;
    static if(is(T == Counts)){
        counts = v;
    }
    else static assert(0, "typeerror");
    Node[] nodes = counts.keys
        .sort!((a,b) => strncmp(cast(const char*)a.ptr,cast(const char*)b.ptr, a.length)>=0)
        .map!(k => new Node(k, counts[k])).array
        .sort!((a,b) => a.count > b.count).array;
    while (nodes.length>1) {
        Node[] tmpNodes = new Node[nodes.length/2];
        ulong tmpi = tmpNodes.length;
        while (tmpi >0 && nodes.length >1 ){
            tmpi--;
            tmpNodes[tmpi] = new Node(nodes[$-2], nodes[$-1]);
            nodes.length -= 2;
        }
        nodes ~= tmpNodes;
    }
    
    return nodes[0];
}

auto alphabet(T)(Node head){
    T[ubytes] m;
    void recursion(Node h, T path = []){
        if(h.content){
            m[h.value] = path;
            return;
        }
        static if(is(T == bools)){
            enum right = true;
            enum left = false;
        }
        else static if (is(T ==string)){
            enum right = '1';
            enum left = '0';
        
        }
        else static assert(0, "Not supported type");
        if(h.right) recursion(h.right, path~ right);
        if(h.left) recursion(h.left, path~left);
    }
    recursion(head);
    return m;
}

/// Reads file and write compressed data to archive
auto compres(string fileName, string archiveName, ulong byteLen=1) {
    import std.file:read;
    import std.stdio: File, writeln;
    import bitarray:BitArray;

    // Compressing File
    auto data = cast(ubytes) fileName.read;

    auto counts = data.countBytes(byteLen);
    auto tree = counts.buildHaffmanTree;
    bools[ubytes] _alphabet = tree.alphabet!bools;
    BitArray compressedData = new BitArray([]);
    foreach(i;0..data.length/byteLen){
         auto w = data[i*byteLen..(i+1)*byteLen];
         foreach(c; _alphabet[w]){
            compressedData ~= c;
         }
    }

    // Writing to new file
    auto archive = archiveName.File("wb");
    archive.write("\0HAFF\0");          // label of file
    archive.rawWrite([counts.length, byteLen]); //writing info for build tree
    foreach(w, count; counts){
        archive.rawWrite(w);
        archive.rawWrite([count]);
    }
    compressedData.write(archive);
    auto len = data.length/byteLen*byteLen;
    if(len < data.length)    
        archive.rawWrite(data[len..$]);
    archive.close;
    return tuple(counts, tree);
}


auto decompres(string archiveName, string fileName){
    import std.stdio: File,writeln;
    import bitarray:BitArray;

    // Start reading archive
    auto archive = archiveName.File("rb");
    char[6] label;
    archive.rawRead(label);
    assert(label == "\0HAFF\0", "File not parseable");
    
    //Reading tree
    Counts counts;
    ulong[1] _countsLen;
    ulong[1] _byteLen;
    archive.rawRead(_countsLen);
    archive.rawRead(_byteLen);
    foreach(_;0.._countsLen[0]){
        ubyte[] c = new ubyte[_byteLen[0]];
        ulong[1] count;
        archive.rawRead(c);
        archive.rawRead(count);

        counts[cast(ubytes)c[]] = count[0];
    }

    auto tree = counts.buildHaffmanTree;
    
    //Decompressing
    auto compressedData = new BitArray([]);
    compressedData.read(archive);
    auto tail = archive.rawRead(new ubyte[_byteLen[0]]);
    archive.close;
    File file = fileName.File("wb");
    Node cur = tree;
    foreach(bit; compressedData){
        if (bit){
            cur = cur.right;    
        }
        else{
            cur = cur.left;
        }
        if(cur.content){
            file.rawWrite(cur.value);
            cur = tree;
        }
    }
    file.rawWrite(tail);
    file.close;
    return tuple(counts, tree);
}

unittest {
    import std;
    
    Counts c = countBytes(cast(ubytes)"123 132 123 велосипед123 123 3213221", 2);
    c.writeln;
    auto t = buildHaffmanTree(c); 
    t.writeln;
    auto a = alphabet!string(t);
    a.writeln;

}

unittest {
    Counts a = countBytes(cast(ubytes)"123 132 123 велосипед123 123 3213221", 2);
    Counts b = countBytes(cast(ubytes)"123 132 123 велосипед123 123 3213221", 2);
    Counts c = countBytes(cast(ubytes)"123 132 123 велоqweсипед123 123 3213221", 2);
    assert(a == b);
    assert(a != c);
    assert(b != c);

    Node ta = a.buildHaffmanTree;
    Node tb = b.buildHaffmanTree;
    Node tc = c.buildHaffmanTree;
    
    assert(ta.toString == tb.toString);
    assert(ta.toString != tc.toString);
    assert(tb.toString != tc.toString);
}
